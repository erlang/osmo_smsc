% SM-TP (Short Message Transport Protocol) codec unit tests.
%
% (C) 2020 by Vadim Yanitskiy <axilirator@gmail.com>
%
% All Rights Reserved
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation; either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Additional Permission under GNU AGPL version 3 section 7:
%
% If you modify this Program, or any covered work, by linking or
% combining it with runtime libraries of Erlang/OTP as released by
% Ericsson on http://www.erlang.org (or a modified version of these
% libraries), containing parts covered by the terms of the Erlang Public
% License (http://www.erlang.org/EPLICENSE), the licensors of this
% Program grant you additional permission to convey the resulting work
% without the need to license the runtime libraries of Erlang/OTP under
% the GNU Affero General Public License. Corresponding Source for a
% non-source form of such a combination shall include the source code
% for the parts of the runtime libraries of Erlang/OTP used as well as
% that of the covered work.

-module(sm_tp_codec_test).

-include_lib("eunit/include/eunit.hrl").

%% Rest octets following TP-Address field
-define(TEST_TP_DUMMY_REST, << 16#de, 16#ad, 16#be, 16#ef >>).

%% Alphanumeric address "MTC" (3 * 7 useful bits, 3 * 8 bits with padding)
-define(TEST_TP_ADDR_ANUM_T, {02#0000, 02#101}).
-define(TEST_TP_ADDR_ANUM_V, << 16#4d, 16#ea, 16#10 >>).
-define(TEST_TP_ADDR_ANUM_LV, {3 * 7, ?TEST_TP_ADDR_ANUM_V}).
-define(TEST_TP_ADDR_ANUM_TLV, ?TEST_TP_ADDR_ANUM_T, ?TEST_TP_ADDR_ANUM_LV).

-define(TEST_TP_ADDR_ANUM_ENC, << 16#06, 16#d0, ?TEST_TP_ADDR_ANUM_V/bytes >>).
-define(TEST_TP_ADDR_ANUM_LV_DEC, {3 * 8, ?TEST_TP_ADDR_ANUM_V}).
-define(TEST_TP_ADDR_ANUM_TLV_DEC, ?TEST_TP_ADDR_ANUM_T, ?TEST_TP_ADDR_ANUM_LV_DEC).

%% BCD-encoded odd address "+79137081000" (11 * 4 useful bits, 12 * 4 bits with padding)
-define(TEST_TP_ADDR_ONUM_T, {02#0001, 02#001}).
-define(TEST_TP_ADDR_ONUM_V, << 16#97, 16#31, 16#07, 16#18, 16#00, 16#f0 >>).
-define(TEST_TP_ADDR_ONUM_LV, {11 * 4, ?TEST_TP_ADDR_ONUM_V}).
-define(TEST_TP_ADDR_ONUM_TLV, ?TEST_TP_ADDR_ONUM_T, ?TEST_TP_ADDR_ONUM_LV).
-define(TEST_TP_ADDR_ONUM_ENC, << 16#0b, 16#91, ?TEST_TP_ADDR_ONUM_V/bytes >>).

%% BCD-encoded even address "123456" (6 * 4 useful bits, no padding)
-define(TEST_TP_ADDR_ENUM_T, {02#1000, 02#000}).
-define(TEST_TP_ADDR_ENUM_V, << 16#21, 16#43, 16#65 >>).
-define(TEST_TP_ADDR_ENUM_LV, {6 * 4, ?TEST_TP_ADDR_ENUM_V}).
-define(TEST_TP_ADDR_ENUM_TLV, ?TEST_TP_ADDR_ENUM_T, ?TEST_TP_ADDR_ENUM_LV).
-define(TEST_TP_ADDR_ENUM_ENC, << 16#06, 16#88, ?TEST_TP_ADDR_ENUM_V/bytes >>).

encode_tp_addr_test() ->
	?assertEqual(?TEST_TP_ADDR_ANUM_ENC, sm_tp_codec:encode_tp_addr(?TEST_TP_ADDR_ANUM_TLV)),
	?assertEqual(?TEST_TP_ADDR_ONUM_ENC, sm_tp_codec:encode_tp_addr(?TEST_TP_ADDR_ONUM_TLV)),
	?assertEqual(?TEST_TP_ADDR_ENUM_ENC, sm_tp_codec:encode_tp_addr(?TEST_TP_ADDR_ENUM_TLV)).

decode_tp_addr_test() ->
	%% Alphanumeric (GSM 7 bit encoding) address "MTC"
	?assertEqual({ok, ?TEST_TP_ADDR_ANUM_TLV_DEC, << >>},
		     sm_tp_codec:decode_tp_addr(?TEST_TP_ADDR_ANUM_ENC)),
	?assertEqual({ok, ?TEST_TP_ADDR_ANUM_TLV_DEC, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_addr(<< ?TEST_TP_ADDR_ANUM_ENC/bytes,
						   ?TEST_TP_DUMMY_REST/bytes >>)),

	%% BCD-encoded odd address "+79137081000"
	?assertEqual({ok, ?TEST_TP_ADDR_ONUM_TLV, << >>},
		     sm_tp_codec:decode_tp_addr(?TEST_TP_ADDR_ONUM_ENC)),
	?assertEqual({ok, ?TEST_TP_ADDR_ONUM_TLV, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_addr(<< ?TEST_TP_ADDR_ONUM_ENC/bytes,
						   ?TEST_TP_DUMMY_REST/bytes >>)),

	%% BCD-encoded even address "123456"
	?assertEqual({ok, ?TEST_TP_ADDR_ENUM_TLV, << >>},
		     sm_tp_codec:decode_tp_addr(?TEST_TP_ADDR_ENUM_ENC)),
	?assertEqual({ok, ?TEST_TP_ADDR_ENUM_TLV, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_addr(<< ?TEST_TP_ADDR_ENUM_ENC/bytes,
						   ?TEST_TP_DUMMY_REST/bytes >>)).

-define(TEST_TP_SCTS_TIME0, {{00, 00, 00}, {00, 00, 00}}). % Intentionally incorrect
-define(TEST_TP_SCTS_TIME1, {{70, 04, 22}, {11, 10, 01}}). % Lenin's birthday!
-define(TEST_TP_SCTS_TIME2, {{69, 12, 01}, {22, 45, 33}}). % Just a random date

-define(TEST_TP_SCTS_TIME0_ENC, << 16#00, 16#00, 16#00, 16#00, 16#00, 16#00 >>).
-define(TEST_TP_SCTS_TIME1_ENC, << 16#07, 16#40, 16#22, 16#11, 16#01, 16#10 >>).
-define(TEST_TP_SCTS_TIME2_ENC, << 16#96, 16#21, 16#10, 16#22, 16#54, 16#33 >>).

-define(TEST_TP_SCTS_TZ_00_ENC,		16#00). % GMT+0
-define(TEST_TP_SCTS_TZ_40_ENC,		16#61). % GMT+4
-define(TEST_TP_SCTS_TZ_F715_ENC,	16#9a). % GMT-7,15
-define(TEST_TP_SCTS_TZ_F30_ENC,	16#29). % GMT-3
-define(TEST_TP_SCTS_TZ_1230_ENC,	16#05). % GMT+12,30

encode_tp_scts_test() ->
	?assertEqual(<< ?TEST_TP_SCTS_TIME0_ENC:6/bytes, ?TEST_TP_SCTS_TZ_00_ENC >>,
		     sm_tp_codec:encode_tp_scts(?TEST_TP_SCTS_TIME0, {0, 0})),
	?assertEqual(<< ?TEST_TP_SCTS_TIME1_ENC:6/bytes, ?TEST_TP_SCTS_TZ_40_ENC >>,
		     sm_tp_codec:encode_tp_scts(?TEST_TP_SCTS_TIME1, {4, 0})),
	?assertEqual(<< ?TEST_TP_SCTS_TIME2_ENC:6/bytes, ?TEST_TP_SCTS_TZ_F715_ENC >>,
		     sm_tp_codec:encode_tp_scts(?TEST_TP_SCTS_TIME2, {-7, 15})),
	?assertEqual(<< ?TEST_TP_SCTS_TIME1_ENC:6/bytes, ?TEST_TP_SCTS_TZ_F30_ENC >>,
		     sm_tp_codec:encode_tp_scts(?TEST_TP_SCTS_TIME1, {-3, 0})),
	?assertEqual(<< ?TEST_TP_SCTS_TIME2_ENC:6/bytes, ?TEST_TP_SCTS_TZ_1230_ENC >>,
		     sm_tp_codec:encode_tp_scts(?TEST_TP_SCTS_TIME2, {12, 30})),

	?assertError(_, % TODO: {error, function_clause, ...} due to YY > 99 and HH > 99
		     sm_tp_codec:encode_tp_scts({{1999, 01, 01}, {120, 00, 00}}, {0, 0})),
	?assertError(_, % TODO: {error, function_clause, ...} due to HH < 0
		     sm_tp_codec:encode_tp_scts({{89, 02, 04}, {-5, 00, 00}}, {7, 0})),
	?assertError(_, % TODO: {error, function_clause, ...} due to (TZMin rem 15) =/= 0
		     sm_tp_codec:encode_tp_scts(?TEST_TP_SCTS_TIME1, {5, 5})).

decode_tp_scts_test() ->
	?assertEqual({ok, ?TEST_TP_SCTS_TIME0, {0, 0}},
		     sm_tp_codec:decode_tp_scts(<< ?TEST_TP_SCTS_TIME0_ENC:6/bytes,
						   ?TEST_TP_SCTS_TZ_00_ENC >>)),
	?assertEqual({ok, ?TEST_TP_SCTS_TIME1, {4, 0}},
		     sm_tp_codec:decode_tp_scts(<< ?TEST_TP_SCTS_TIME1_ENC:6/bytes,
						   ?TEST_TP_SCTS_TZ_40_ENC >>)),
	?assertEqual({ok, ?TEST_TP_SCTS_TIME2, {-7, 15}},
		     sm_tp_codec:decode_tp_scts(<< ?TEST_TP_SCTS_TIME2_ENC:6/bytes,
						   ?TEST_TP_SCTS_TZ_F715_ENC >>)),
	?assertEqual({ok, ?TEST_TP_SCTS_TIME1, {-3, 0}},
		     sm_tp_codec:decode_tp_scts(<< ?TEST_TP_SCTS_TIME1_ENC:6/bytes,
						   ?TEST_TP_SCTS_TZ_F30_ENC >>)),
	?assertEqual({ok, ?TEST_TP_SCTS_TIME2, {12, 30}},
		     sm_tp_codec:decode_tp_scts(<< ?TEST_TP_SCTS_TIME2_ENC:6/bytes,
						   ?TEST_TP_SCTS_TZ_1230_ENC >>)).

%% TP-Validity-Period is absent.
-define(TEST_TP_VP_OMIT,	02#00, ?TEST_TP_DUMMY_REST).

%% 9.2.3.12.1 TP-Validity-Period (Relative format).
-define(TEST_TP_VP_RELATIVE0,	02#10, << 16#00, ?TEST_TP_DUMMY_REST/bytes >>). % 5 minutes
-define(TEST_TP_VP_RELATIVE1,	02#10, << 16#8f, ?TEST_TP_DUMMY_REST/bytes >>). % 12 hours
-define(TEST_TP_VP_RELATIVE2,	02#10, << 16#a7, ?TEST_TP_DUMMY_REST/bytes >>). % 24 hours
-define(TEST_TP_VP_RELATIVE3,	02#10, << 16#a8, ?TEST_TP_DUMMY_REST/bytes >>). % 2 days
-define(TEST_TP_VP_RELATIVE4,	02#10, << 16#ff, ?TEST_TP_DUMMY_REST/bytes >>). % 63 weeks

%% 9.2.3.12.2 TP-Validity-Period (Absolute format).
-define(TEST_TP_VP_ABSOLUTE1,	02#11, << ?TEST_TP_SCTS_TIME1_ENC:6/bytes,
					  ?TEST_TP_SCTS_TZ_40_ENC >>).
-define(TEST_TP_VP_ABSOLUTE2,	02#11, << ?TEST_TP_SCTS_TIME2_ENC:6/bytes,
					  ?TEST_TP_SCTS_TZ_F30_ENC >>).

%% 9.2.3.12.3 TP-Validity-Period (Enhanced format).
-define(TEST_TP_VP_ENHANCED0,	02#01, << 16#00, 16#00, 16#00, 16#00, 16#00, 16#00, 16#00,
					  ?TEST_TP_DUMMY_REST/bytes >>). % not present
-define(TEST_TP_VP_ENHANCED1,	02#01, << 16#41, 16#01, 16#00, 16#00, 16#00, 16#00, 16#00,
					  ?TEST_TP_DUMMY_REST/bytes >>). % relative, 10 minutes
-define(TEST_TP_VP_ENHANCED2,	02#01, << 16#42, 16#1e, 16#00, 16#00, 16#00, 16#00, 16#00,
					  ?TEST_TP_DUMMY_REST/bytes >>). % relative, 30 seconds
-define(TEST_TP_VP_ENHANCED3,	02#01, << 16#03, 16#00, 16#22, 16#71, 16#00, 16#00, 16#00,
					  ?TEST_TP_DUMMY_REST/bytes >>). % semi-octet, 00:22:17

-define(TEST_TP_VP_ENHANCED4,	02#01, << 16#47, 16#00, 16#00, 16#00, 16#00, 16#00, 16#00,
					  ?TEST_TP_DUMMY_REST/bytes >>). % reserved EVPF
-define(TEST_TP_VP_ENHANCED5,	02#01, << 16#80, 16#00, 16#00, 16#00, 16#00, 16#00, 16#00,
					  ?TEST_TP_DUMMY_REST/bytes >>). % extension bit=1

-define(TEST_TP_VPF_UNKNOWN,	16#ff, << ?TEST_TP_DUMMY_REST/bytes >>).

decode_tp_vp_test() ->
	?assertEqual({ok, omit, << >>},
		     sm_tp_codec:decode_tp_vp(02#00, << >>)),
	?assertEqual({ok, omit, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_OMIT)),

	?assertEqual({ok, {relative, 5 * 60}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_RELATIVE0)),
	?assertEqual({ok, {relative, 12 * 3600}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_RELATIVE1)),
	?assertEqual({ok, {relative, 24 * 3600}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_RELATIVE2)),
	?assertEqual({ok, {relative, 2 * 24 * 3600}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_RELATIVE3)),
	?assertEqual({ok, {relative, 63 * 7 * 24 * 3600}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_RELATIVE4)),

	?assertEqual({ok, {absolute, ?TEST_TP_SCTS_TIME1, {4, 0}}, << >>},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ABSOLUTE1)),
	?assertEqual({ok, {absolute, ?TEST_TP_SCTS_TIME2, {-3, 0}}, << >>},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ABSOLUTE2)),

	?assertEqual({ok, {enhanced, 0, omit}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ENHANCED0)),
	?assertEqual({ok, {enhanced, 1, {relative, 600}}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ENHANCED1)),
	?assertEqual({ok, {enhanced, 1, {relative, 30}}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ENHANCED2)),
	?assertEqual({ok, {enhanced, 0, {relative, 1337}}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ENHANCED3)),

	?assertEqual({ok, {enhanced, 1, reserved}, ?TEST_TP_DUMMY_REST},
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ENHANCED4)),
	?assertError({vpf_ext_not_supported}, % extension bit=1 not supported
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VP_ENHANCED5)),
	?assertError({unknown_vpf, 16#ff}, % unknown TP-Validity-Period-Format
		     sm_tp_codec:decode_tp_vp(?TEST_TP_VPF_UNKNOWN)).
