% BCD (Binary-Coded Decimal) codec unit tests.
% https://en.wikipedia.org/wiki/Binary-coded_decimal
%
% (C) 2020 by Vadim Yanitskiy <axilirator@gmail.com>
%
% All Rights Reserved
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation; either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Additional Permission under GNU AGPL version 3 section 7:
%
% If you modify this Program, or any covered work, by linking or
% combining it with runtime libraries of Erlang/OTP as released by
% Ericsson on http://www.erlang.org (or a modified version of these
% libraries), containing parts covered by the terms of the Erlang Public
% License (http://www.erlang.org/EPLICENSE), the licensors of this
% Program grant you additional permission to convey the resulting work
% without the need to license the runtime libraries of Erlang/OTP under
% the GNU Affero General Public License. Corresponding Source for a
% non-source form of such a combination shall include the source code
% for the parts of the runtime libraries of Erlang/OTP used as well as
% that of the covered work.

-module(bcd_codec_test).

-include_lib("eunit/include/eunit.hrl").

-define(TEST_IMSI_STR, "901701234567890").
-define(TEST_IMSI_NUM, 901701234567890).
-define(TEST_IMSI_ENC, << 16#09, 16#71, 16#10, 16#32, 16#54, 16#76, 16#98, 16#f0 >>).
-define(TEST_IMSI_ENC_NOPAD, << 16#09, 16#71, 16#10, 16#32, 16#54, 16#76, 16#98, 16#0:4 >>).

-define(TEST_USSD_STR, "*#100#").
-define(TEST_USSD_ENC, << 16#ed, 16#01, 16#e0 >>).

encode_test() ->
	?assertEqual(?TEST_IMSI_ENC, bcd_codec:encode(<< ?TEST_IMSI_STR >>)),
	?assertEqual(?TEST_IMSI_ENC, bcd_codec:encode(?TEST_IMSI_STR)),
	?assertEqual(?TEST_IMSI_ENC, bcd_codec:encode(?TEST_IMSI_NUM)),

	?assertEqual(?TEST_USSD_ENC, bcd_codec:encode(<< ?TEST_USSD_STR >>)),
	?assertEqual(?TEST_USSD_ENC, bcd_codec:encode(?TEST_USSD_STR)),

	?assertEqual(<< 16#ba, 16#fc >>, bcd_codec:encode(<< "abc" >>)),
	?assertEqual(<< >>, bcd_codec:encode(<< >>)),
	?assertEqual(<< >>, bcd_codec:encode("")),

	?assertError({bad_bcd_character, $M}, bcd_codec:encode(<< "Mahlzeit!" >>)),
	?assertError({bad_bcd_character, $f}, bcd_codec:encode("880043556f")),
	?assertError(_, bcd_codec:encode(nope)). % TODO: 'no matching function clause'

encode_nopad_test() ->
	?assertEqual(?TEST_IMSI_ENC_NOPAD, bcd_codec:encode_nopad(<< ?TEST_IMSI_STR >>)),
	?assertEqual(?TEST_USSD_ENC, bcd_codec:encode_nopad(<< ?TEST_USSD_STR >>)),
	?assertEqual(<< 16#ba, 16#c:4 >>, bcd_codec:encode_nopad(<< "abc" >>)).

decode_test() ->
	?assertEqual(?TEST_IMSI_STR, bcd_codec:decode(?TEST_IMSI_ENC)),
	?assertEqual(?TEST_USSD_STR, bcd_codec:decode(?TEST_USSD_ENC)),

	?assertEqual("abc", bcd_codec:decode(<< 16#ba, 16#fc >>)),
	?assertEqual("abc", bcd_codec:decode([16#ba, 16#fc])),
	?assertEqual("", bcd_codec:decode(<< >>)),

	%% TODO: expect 'no matching function clause' here
	?assertError(_, bcd_codec:decode_num(<< ?TEST_IMSI_ENC/binary, 16#ff:3 >>)),
	?assertError(_, bcd_codec:decode_num(<< 16#ff:1, ?TEST_USSD_ENC/binary >>)),
	?assertError(_, bcd_codec:encode_num(nope)).

encode_num_test() ->
	?assertEqual(16#99, bcd_codec:encode_num(99)),
	?assertEqual(16#05, bcd_codec:encode_num(50)),
	?assertEqual(16#14, bcd_codec:encode_num(41)),
	?assertEqual(16#10, bcd_codec:encode_num(1)),
	?assertEqual(16#00, bcd_codec:encode_num(0)),

	%% TODO: expect 'no matching function clause' here
	?assertError(_, bcd_codec:encode_num(999)),
	?assertError(_, bcd_codec:encode_num(100)),
	?assertError(_, bcd_codec:encode_num(-1)).

decode_num_test() ->
	?assertEqual(99, bcd_codec:decode_num(16#99)),
	?assertEqual(50, bcd_codec:decode_num(16#05)),
	?assertEqual(41, bcd_codec:decode_num(16#14)),
	?assertEqual(1, bcd_codec:decode_num(16#10)),
	?assertEqual(0, bcd_codec:decode_num(16#00)),

	%% TODO: expect 'no matching function clause' here
	?assertError(_, bcd_codec:decode_num(16#666)),
	?assertError(_, bcd_codec:decode_num(16#a0)),
	?assertError(_, bcd_codec:decode_num(16#0a)),
	?assertError(_, bcd_codec:decode_num(-1)).
