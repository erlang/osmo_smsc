-module(osmo_smsc_app).
-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
	{ok, _} = osmo_smsc_core:start_link().

stop(_State) ->
	ok.
