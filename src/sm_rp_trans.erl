% Simplified SMR FSM for a single SMS transaction (as per 3GPP TS 24.011).
%
% (C) 2019 by Vadim Yanitskiy <axilirator@gmail.com>
%
% All Rights Reserved
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation; either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Additional Permission under GNU AGPL version 3 section 7:
%
% If you modify this Program, or any covered work, by linking or
% combining it with runtime libraries of Erlang/OTP as released by
% Ericsson on http://www.erlang.org (or a modified version of these
% libraries), containing parts covered by the terms of the Erlang Public
% License (http://www.erlang.org/EPLICENSE), the licensors of this
% Program grant you additional permission to convey the resulting work
% without the need to license the runtime libraries of Erlang/OTP under
% the GNU Affero General Public License. Corresponding Source for a
% non-source form of such a combination shall include the source code
% for the parts of the runtime libraries of Erlang/OTP used as well as
% that of the covered work.

-module(sm_rp_trans).
-behaviour(gen_statem).

% gen_statem callbacks
-export([
	 init/1,
	 terminate/3,
	 code_change/4,
	 callback_mode/0
	]).

% State functions (section 6.2.2)
-export([
	 smr_st_idle/3,
	 smr_st_wait_rx_ack/3,
	 smr_st_wait_tx_ack/3
	]).

% API functions
-export([
	 start_link/2,
	 start/2
	]).

% 3GPP TS 24.011, chapter 10: Timers
-define(GSM411_TMR_TR1N, 40 * 1000).
-define(GSM411_TMR_TRAN, 30 * 1000).
-define(GSM411_TMR_TR2N, 15 * 1000).

-record(smr_data, {imsi, msg_ref, orig_addr, dest_addr, tpdu}).
-record(smr_state, {iface, core, data}).

%% API functions

start_link(Interface, Core) ->
	gen_statem:start_link(?MODULE, [Interface, Core], []).

start(Interface, Core) ->
	gen_statem:start(?MODULE, [Interface, Core], []).

%% gen_statem callbacks

init([Interface, Core]) ->
	{ok, smr_st_idle, #smr_state{iface = Interface, core = Core, data = nope}}.

% Define a separate handler function for each state
callback_mode() ->
	state_functions.

% Rx MO RP-DATA from the MSC, forward to the SMSC
smr_st_idle({call, From}, {smr_ev_fwd_mo_sm_req, Data}, State) ->
	{next_state, smr_st_wait_tx_ack,
		State#smr_state{data = Data},
		[{reply, From, ok}, {state_timeout, ?GSM411_TMR_TR2N, wait_tx_ack}]};
% Rx MO RP-SMMA from the MSC, notify the SMSC
smr_st_idle({call, From}, {smr_ev_fwd_mo_smma_req, Data}, State) ->
	{next_state, smr_st_wait_tx_ack,
		State#smr_state{data = Data},
		[{reply, From, ok}, {state_timeout, ?GSM411_TMR_TR2N, wait_tx_ack}]};
% Rx MT RP-DATA from the SMSC, forward to the MSC
smr_st_idle({call, From}, {smr_ev_fwd_mt_sm_req, Data}, State) ->
	gen_server:cast(State#smr_state.iface, {smsc_ev_mt_fwd_sm_req, Data}),
	{next_state, smr_st_wait_rx_ack,
		State#smr_state{data = Data},
		[{reply, From, ok}, {state_timeout, ?GSM411_TMR_TR1N, wait_rx_ack}]};
% Trap for unhandled events
smr_st_idle(EventType, EventContent, State) ->
	io:format("Event ~p (~p) unhandled in state IDLE~n",
		  [EventContent, EventType]),
	{keep_state, State}.

% Rx MO RP-ACK from the SMSC, forward to the MSC
smr_st_wait_tx_ack({call, From}, smr_ev_fwd_mo_sm_ack, State) ->
	SMRData = State#smr_state.data, MR = SMRData#smr_data.msg_ref,
	gen_server:cast(State#smr_state.iface,
		{smsc_ev_mo_fwd_sm_ack,
		 SMRData#smr_data.imsi,
		 SMRData#smr_data.msg_ref}),
	{stop, normal, State};
% Rx MO RP-ERROR from the SMSC, forward to the MSC
smr_st_wait_tx_ack({call, From}, {smr_ev_fwd_mo_sm_err, Cause}, State) ->
	SMRData = State#smr_state.data,
	gen_server:cast(State#smr_state.iface,
		{smsc_ev_mo_fwd_sm_err,
		 SMRData#smr_data.imsi,
		 SMRData#smr_data.msg_ref,
		 Cause}),
	{stop, normal, State};
% Timeout waiting for RP-ACK / RP-ERROR
smr_st_wait_tx_ack(state_timeout, wait_tx_ack, S) ->
	% TODO: we probably want to try again here
	% TODO: notify the MSC/MS about that
	io:format("Timeout waiting for RP-ACK / RP-ERROR for MO SMS~n"),
	{stop, timeout, S};
smr_st_wait_tx_ack(EventType, EventContent, State) ->
	io:format("Event ~p (~p) unhandled in state WAIT_TX_ACK~n",
		  [EventContent, EventType]),
	{keep_state, State}.

% Rx MT RP-ACK from the MSC, forward to the SMSC
smr_st_wait_rx_ack({call, From}, smr_ev_fwd_mt_sm_ack, S) ->
	{stop, normal, S};
% Rx MT RP-ERROR from the MSC, forward to the SMSC
smr_st_wait_rx_ack({call, From}, {smr_ev_fwd_mt_sm_err, _Cause}, S) ->
	{stop, normal, S};
% Timeout waiting for RP-ACK / RP-ERROR
smr_st_wait_rx_ack(state_timeout, wait_rx_ack, S) ->
	% TODO: we probably want to try again here
	% TODO: notify the SMSC about that
	io:format("Timeout waiting for RP-ACK / RP-ERROR for MT SMS~n"),
	{stop, timeout, S};
smr_st_wait_rx_ack(EventType, EventContent, State) ->
	io:format("Event ~p (~p) unhandled in state WAIT_RX_ACK~n",
		  [EventContent, EventType]),
	{keep_state, State}.

terminate(Reason, StateName, _State) ->
	io:format("Terminating in state ~p (reason ~p)~n", [StateName, Reason]),
	ok.

code_change(_OldVsn, StateName, State, _Extra) ->
	{ok, StateName, State}.
